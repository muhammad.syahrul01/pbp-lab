1. Apakah perbedaan antara JSON dan XML?
Terdapat beberapa perbedaan antara JSON dan XML. Berikut perbedaannya.
- XML merupakan bahasa markup, sedangkan JSON merupakan format yang ditulis dalam JavaScript
- permintaan AJAX pada XML lebih lambat sebab banyaknya bandwidth, sedangkan permintaan AJAX pada JSON lebih cepat karena datanya diproses secara serial
- XML mendukung beberapa hal (UTF-8, UTF-16, tipe data kompleks, namespaces, komentar, dan metadata), sedangkan JSON mendukung (UTF, ASCII, string, angka, array boolean, dan objek tipe primitif)
- XML membutuhkan tag untuk mendukung array, sedangkan JSON sudah mendukung pengaksesan array
- XML mempunyai ukuran file besar, sedangkan JSON memiliki ukuran file sederhana
- Dikarenakan penguraian yang lambat mengakibatkan transmisi data XML lebih lambat, sementara itu penguraian pada JSON lebih cepat sehingga transmisi data juga lebih cepat
- XML berorientasi pada dokumen, sedangkan JSON berorientasi pada data
- Syntax pada XML lebih rumit dan cukup susah dipahami, sedangkan syntax pada JSON lebih sederhana dan mudah dipahami
- Data XML disimpan sebagai tree structure, sedangkan JSON disimpan sebagai map yang terdapat key dan value

2. Apakah perbedaan antara HTML dan XML?
Berikut perbedaan antara HTML dan XML.
- Tag XML tidak ditentukan sebelumnya, sedangkan HTML sudah memiliki tag yang ditentukan sebelumnya
- Tag XML dapat dikembangkan, sementara itu HTML memiliki tag terbatas
- XML sangat strict terhadap tag penutup, sedangkan HTML tidak strict
- XML merupakan singkatan dari eXtensible Markup Language, sedangkan HTML merupakan singkatan dari Hypertext Markup Language
- XML berpusat pada transfer data, sedangkan HTML berpusat pada penyajian data
- XML didukung konten, sedangkan XML didukung format
- XML itu case sensitive, sedangkan HTML itu case insensitive
- XML menyediakan dukungan namespaces, sementara itu HTML tidak menyediakan dukungan namespaces
- dalam fungsinya XML membantu untuk bertukar data antar platform, sedangkan HTML membantu mengembangkan struktur halaman web 