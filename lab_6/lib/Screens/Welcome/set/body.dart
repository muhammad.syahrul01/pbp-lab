import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/Signup/tampilan_daftar.dart';
import 'package:flutter_auth/Screens/Welcome/set/background.dart';
import 'package:flutter_auth/Screens/Welcome/tampilan%20welcome.dart';
import 'package:flutter_auth/components/flat_button.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "HelPINK U",
              style: TextStyle(color: Color(0xFFC37B89), fontSize: 24),
            ),
            SizedBox(height: size.height * 0.06),
            RoundedButton(
              text: "MASUK",
              textColor: Colors.amber[700],
              color: Color(0xFFC37B89),
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return WelcomeScreen();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "DAFTAR",
              textColor: Colors.amber[700],
              color: Color(0xFFC37B89),
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
