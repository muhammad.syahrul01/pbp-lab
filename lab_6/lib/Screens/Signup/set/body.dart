import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/Signup/set/background.dart';
import 'package:flutter_auth/Screens/Welcome/tampilan%20welcome.dart';
import 'package:flutter_auth/components/cek_akun.dart';
import 'package:flutter_auth/components/flat_button.dart';
import 'package:flutter_auth/components/email_field.dart';
import 'package:flutter_auth/components/username_field.dart';
import 'package:flutter_auth/components/password_field.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "DAFTAR AKUN",
              style: TextStyle(color: Colors.white, fontSize: 27),
            ),
            SizedBox(height: size.height * 0.04),
            RoundedInputField(
              hintText: "Username...",
              onChanged: (value) {},
            ),
            Text(
                "Username hanya dapat terdiri dari huruf, angka, dan @/./+/-/_ saja"),
            RoundedEmailField(
              hintText: "Email...",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              hintText: "Enter Password...",
              onChanged: (value) {},
            ),
            Text("- Password harus mengandung setidaknya 8 karakter"),
            Text("Password tidak boleh mirip dengan data yang lain"),
            Text("Password tidak boleh yang sering digunakan"),
            Text("Password tidak boleh seluruhnya angka"),
            RoundedPasswordField(
              hintText: "Re-enter Password...",
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "DAFTAR",
              color: Color(0xFFF3F4ED),
              press: () {},
            ),
            cekAkun(
              question: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return WelcomeScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
