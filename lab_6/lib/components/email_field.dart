import 'package:flutter/material.dart';

class RoundedEmailField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  final Widget child;
  const RoundedEmailField({
    Key key,
    this.hintText,
    this.icon = Icons.email,
    this.onChanged,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return TextFieldContainer(
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 1),
      width: size.width * 0.5,
      decoration: BoxDecoration(
        color: Colors.white,
        // borderRadius: BorderRadius.circular(29),
      ),
      child: TextField(
        onChanged: onChanged,
        obscuringCharacter: "@",
        cursorColor: Colors.black,
        decoration: InputDecoration(
          icon: Icon(
            Icons.email_outlined,
            color: Colors.amber[700],
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
