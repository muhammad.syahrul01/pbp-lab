from django.urls import path
from .views import index, addFriend

urlpatterns = [
    path('', index, name='index'),
    path('add', addFriend, name='add')
]