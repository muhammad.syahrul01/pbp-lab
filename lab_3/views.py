from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url= "/admin/login/")
def index(request):
    friends = Friend.objects.all().values()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def addFriend(request):
    if request.method == "POST":
        temp = FriendForm(request.POST)
        if temp.is_valid() :
            temp.save()
            return HttpResponseRedirect("/lab-3")
    return render(request, 'lab3_form.html')