from django.db.models.base import Model
from django.forms import ModelForm
from lab_1.models import Friend

class FriendForm (ModelForm) :
    class Meta :
        model = Friend
        fields = "__all__"
