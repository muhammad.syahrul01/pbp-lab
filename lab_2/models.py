from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    from2 = models.CharField(max_length=30)
    title = models.CharField(max_length=15)
    message = models.TextField()