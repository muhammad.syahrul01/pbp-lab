from lab_2.models import Note
from django import forms
from django.forms import fields

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'